// name   : gettext_html_auto.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3

'use strict'

import i18n from './gettext.js'

const prefix = '/html_locales/'

export function walk_text_nodes(apply_on_nodes){
	const walk = document.createTreeWalker(document, NodeFilter.SHOW_TEXT, { acceptNode:
		(node) => { if(! /^\s*$/.test(node.data)) return NodeFilter.FILTER_ACCEPT}}, false)
	var n
	while(n = walk.nextNode()) apply_on_nodes(n)
}
export function query_attr_nodes(attrs, apply_on_nodes){
	for (let a of attrs)
		for (let n of document.querySelectorAll(`[${a}]`))
			apply_on_nodes(n, a)
}
export async function gettext_html_auto(wanted_locale, default_locale='en') {
	var gettext_i18n = i18n()
	gettext_i18n.setLocale(wanted_locale)
	wanted_locale = wanted_locale.slice(0, 2)
	if (wanted_locale == default_locale)
		return gettext_i18n
	try {
		let locale_json = await fetch(`${prefix}${wanted_locale}.json`)
		locale_json = await locale_json.json()
		gettext_i18n.loadJSON(locale_json, 'messages')
	} catch (exc) {
		console.error(`Can't load: ${prefix}${wanted_locale}.json`)
		console.error(exc)
		return gettext_i18n
	}
	try {
		walk_text_nodes((n) => {n.data=gettext_i18n.gettext(n.data)})
		query_attr_nodes(['title', 'alt', 'href', 'placeholder'],
			(n, attr) => {n[attr]=gettext_i18n.gettext(n[attr])})
	} catch (exc) {
		console.error(exc)
	}
	return gettext_i18n
}
export async function xgettext_html(locale='en'){
	try {
		var tpl_file = await fetch(`${prefix}template.json`)
		var tpl_json = await tpl_file.json()
		var black_list = await fetch(`${prefix}black_list.json`)
		black_list = Object.keys(await black_list.json())
	} catch(exc) {
		console.error(exc)
		console.error(`template.json or black_list.json not in ${prefix}`)
		return
	}
	var collected_keys = {
		'': {
			'language': locale,
			'plural-forms': 'nplurals=2; plural=(n!=1);'
		},
	}
	var raw_keys = {}
	walk_text_nodes( (n) => { raw_keys[n.data] = '' })
	query_attr_nodes(['title', 'alt', 'placeholder'], // for button names, use <button>name</but…
		(n, attr) => {
			if (typeof(n[attr]) == 'undefined')
				console.error('Undefined string in', n, 'for attribute', attr)
			raw_keys[n[attr]] = ''
		})
	query_attr_nodes(['href'],
		(n, attr) => {
			if(n[attr].startsWith('http'))
				raw_keys[n[attr]] = ''
		})
	let tpl_key_nb = 0
	tpl_key_nb = Object.keys(tpl_json).length
	for (let k of Object.keys(tpl_json))
		raw_keys[k] = tpl_json[k]
	raw_keys = Object.keys(raw_keys).filter(e => ! black_list.includes(e))
	let key_nb = raw_keys.length
	if (key_nb > tpl_key_nb) {
		for (let k of raw_keys)
			collected_keys[k] = ''
		var a_str = JSON.stringify(collected_keys, null, 2)
		let a = document.createElement("a")
		a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(a_str)}`
		a.download=`template.json`
		a.click()
	} else {
		console.info('No new string detected.')
	}
}
