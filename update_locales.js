// name   : update_locales.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3

/* globals require */
const fs = require('fs')
const prefix = 'html_locales/'
var locales = []
fs.readdirSync(prefix).forEach(file => { locales.push(file) })
const not_locales = ['template.json', 'black_list.json']
locales = locales.filter(e => ! not_locales.includes(e))
const tpl_json = JSON.parse(fs.readFileSync(`${prefix}template.json`, 'utf8'))
const tpl_list = Object.keys(tpl_json)
for (let l of locales) {
	var l_json = JSON.parse(fs.readFileSync(`${prefix}${l}`, 'utf8'))
	var l_list = Object.keys(l_json)
	var lost_keys = l_list.filter(e => ! tpl_list.includes(e))
	var new_json = {}
	for (let k of tpl_list)
		new_json[k] = l_json[k] || tpl_json[k]
	if (lost_keys.length > 0) {
		var lost_keys_json = {}
		for (let k of lost_keys)
			lost_keys_json[k] = l_json[k]
		const dt = new Date().toISOString().replace('T','_').replace(':','h').split(':')[0]
		fs.writeFileSync(`lost_keys_${dt}_${l}`, JSON.stringify(lost_keys_json, null, 2))
	}
	fs.writeFileSync(`${prefix}${l}`, JSON.stringify(new_json, null, 2))
}
